//
//  WeatherModel.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import Foundation
import Combine

class WeatherViewModel: ObservableObject {
    private var weatherService: WeatherService!
    
    @Published var mainWeather = MainWeather()
    @Published var weather = Weather()
    @Published var weatherHourlyList = [HourlyWeather]()
    @Published var nameCity: String = ""
    @Published var list8DaysWeather = [EightDayList]()
    @Published var weatherResponse1: WeatherResponse
    
    init() {
        self.weatherService = WeatherService()
    }
    
    var cityName: String {
        return self.nameCity
    }
    
    var temperature: String {
        if let temp = self.mainWeather.temp {
            return String(format: "%.0f", temp)
        } else {
            return ""
        }
    }
    
    var temperatureMin: String {
        if var temp = self.mainWeather.temp_min {
            temp.round(.toNearestOrAwayFromZero)
            return "\(Int(temp))"
        } else {
            return ""
        }
    }
        
    var temperatureMax: String {
        if let temp = self.mainWeather.temp_max {
            return String(format: "%.0f", temp)
        } else {
            return ""
        }
    }
    
    var descriptionWeather: String {
        self.weather.weatherDescription ?? ""
    }
    
    var listWeatherHourly: [HourlyWeather] {
        self.weatherHourlyList
    }
    
    var listWeatherIn8Days: [EightDayList] {
        self.list8DaysWeather
    }
    
    var nameSearch: String = ""
    var latSearch: String = ""
    var lonSearch: String = ""
    
    func searchByCoor() {
        if let lat = self.latSearch.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), let lon = self.lonSearch.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            fetchWeatherByCoor(lat: lat, lon: lon)
            fetchWeatherHourlyByCoor(lat: lat, lon: lon)
            fetchWeather8DaysByCoor(lat: lat, lon: lon)
        }
    }
    
    private func fetchWeather8DaysByCoor(lat: String, lon: String) {
        self.weatherService.get8WeatherDayByCoordinate(lat: lat, lon: lon) { response in
            if let response = response {
                DispatchQueue.main.async {
                    self.list8DaysWeather = response.list
                }
            }
        }
    }
    
    private func fetchWeatherHourlyByCoor(lat: String, lon: String) {
        self.weatherService.getWeatherHourlyByCoordinate(lat: lat, lon: lon) { response in
            if let response = response {
                DispatchQueue.main.async {
                    self.weatherHourlyList = response.list
                }
            }
        }
    }
    
    func fetchWeatherByCoor(lat: String, lon: String) {
        self.weatherService.getWeatherByCoordinate(lat: lat, lon: lon) { response in
            if let response = response {
                DispatchQueue.main.async {
                    self.mainWeather = response.main
                    self.nameCity = response.name ?? "Hà Nội"
                    self.weather = response.weather.first ?? Weather()
                }
            }
        }
    }
}


