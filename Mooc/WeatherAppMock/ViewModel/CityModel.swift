//
//  CityModel.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//


import Foundation
import Combine

class CityViewModel: ObservableObject {
    private var appService: AppService!
    
    @Published var cities = [City]()
    
    init() {
        self.appService = AppService()
    }
    
    var listCities: [City] {
        return self.cities
    }
    
    
    func getListCities() {
        self.cities = appService.getDataCities()
    }
}

