//
//  TomorowDay.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//

import SwiftUI

struct TomorowDay: View {
    var body: some View {
        VStack {
            Text("23:14 25 Tháng 08")
                    .foregroundColor(.white)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)
            
            HStack(alignment: .center, spacing: 74) {
                VStack(alignment: .center) {
                    Text("33°")
                        .foregroundColor(.white)
                        .padding(.top, 60)
                        .padding(.bottom, 30)
                        .font(.system(size: 70).bold())
                    Text("Cao nhất 33°")
                        .fontWeight(.medium)
                        .foregroundColor(.white)
                    Text("Thấp nhất 13°")
                        .fontWeight(.medium)
                        .foregroundColor(.white)
                    
                }

                VStack(alignment: .center) {
                    Image( "ic_clear_big")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 128, height: 128, alignment: .center)
                    Text("Nhiều mây")
                        .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                        .fontWeight(.medium)
                        .foregroundColor(.white)
                }
            }

            Rectangle()
                .fill(.white)
                .frame(width: UIScreen.main.bounds.width - 32, height: 2, alignment: .center)
                .padding(.top, 10)
            
            Text("Hằng giờ")
                .foregroundColor(.white)
                .fontWeight(.heavy)
                .font(.system(size: 18))
                .padding()
                .listRowInsets(EdgeInsets())
                .frame(width: UIScreen.main.bounds.width, height: 44, alignment: .center)
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            
            List {
                ForEach(0..<9) { row in
                    HStack(alignment: .center) {
                        Text("\(11 + row)h")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.leading, 18)
                        Spacer()
                        Image((row % 2) == 1 ? "ic_rain" : "ic_humidity")
                            .frame(width: 20, height: 20, alignment: .center)
                        Spacer()
                        Image("humidity")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text((row % 2) == 1 ? "79%" : "99%")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                        Spacer()
                        Image("temperate")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text((row % 2) == 1 ? "37" : "27")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.trailing, 18)
                    }
                }
                .padding()
                .listRowInsets(EdgeInsets())
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            .listStyle(PlainListStyle())
            
        }
        .background(Color(red: 0.01, green: 0.748, blue: 0.652))
    }
}

struct TomorowDay_Previews: PreviewProvider {
    static var previews: some View {
        TomorowDay()
    }
}

