//
//  TodayView.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//


import SwiftUI

struct TodayView: View {
    @State var data: HourlyWeather
    var body: some View {
        VStack {
            Text("23:14 25 Tháng 08")
                    .foregroundColor(.white)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)

            HStack(alignment: .center, spacing: 74) {
                ForEach(data.weather, id: \.id) { item in
                    ItemCellView(item: item)
                }
            }

            Rectangle()
                .fill(.white)
                .frame(width: UIScreen.main.bounds.width - 32, height: 2, alignment: .center)
                .padding(.top, 10)
            
            List {
                Text("Hằng giờ")
                    .foregroundColor(.white)
                    .fontWeight(.heavy)
                    .font(.system(size: 18))
                    .padding()
                    .listRowInsets(EdgeInsets())
                    .frame(width: UIScreen.main.bounds.width, height: 44, alignment: .center)
                    .background(Color(red: 0.01, green: 0.748, blue: 0.652))
                
                ForEach(0..<9) { row in
                    HStack(alignment: .center) {
                        Text("\(11 + row)h")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.leading, 18)
                        Spacer()
                        Image((row % 2) == 0 ? "ic_rain" : "ic_humidity")
                            .frame(width: 20, height: 20, alignment: .center)
                        Spacer()
                        Image("humidity")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text((row % 2) == 0 ? "79%" : "99%")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                        Spacer()
                        Image("temperate")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text((row % 2) == 0 ? "37" : "27")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.trailing, 18)
                    }
                }
                .padding()
                .listRowInsets(EdgeInsets())
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            .listStyle(PlainListStyle())
            
        }
        .background(Color(red: 0.01, green: 0.748, blue: 0.652))
    }
}

//struct TodayView_Previews: PreviewProvider {
//    static var previews: some View {
//        TodayView(data: WeatherResponse(coord: <#T##Coord#>, weather: <#T##[Weather]#>, base: <#T##String?#>, main: <#T##MainWeather#>, visibility: <#T##Int?#>, wind: <#T##Wind#>, clouds: <#T##Clouds#>, dt: <#T##Int?#>, sys: <#T##SysWeather#>, timezone: <#T##Int?#>, id: <#T##Int?#>, name: <#T##String?#>, cod: <#T##Int?#>))
//    }
//}


struct ItemCellView: View {
    var item: Weather
    
    var body: some View {
        VStack(alignment: .center) {
            Image( "ic_rain_big")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 128, height: 128, alignment: .center)
            Text(item.weatherDescription ?? "")
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                .fontWeight(.medium)
                .foregroundColor(.white)
        }
    }
}
