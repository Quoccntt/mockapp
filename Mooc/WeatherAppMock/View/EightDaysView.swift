//
//  EightDaysView.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//

import SwiftUI

struct EightDaysView: View {
    var body: some View {
        List {
            ForEach(0..<10) { row in
            HStack(alignment: .center) {
                VStack(alignment: .leading) {
                    Text(row == 0 ? "Hôm nay" : "\(11 + row)/10/2022")
                        .foregroundColor(.white)
                        .font(.system(size: 16))
                        .fontWeight(.bold)
                    Text(row == 1 ? "Có dông" : ((row % 2) == 0 ? "Mưa" : "Nắng"))
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                        .fontWeight(.bold)
                }.padding(.leading, 18)
                
                Spacer()
                
                Image(row == 1 ? "ic_wind" : ((row % 2) == 0 ? "ic_rain" : "ic_humidity"))
                    .frame(width: 20, height: 20, alignment: .center)
                    .padding(.trailing, 27)
                
                Text(row == 1 ? "97%" : ((row % 2) == 0 ? "98%" : "99%"))
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding(.trailing, 10)
                
                VStack(alignment: .center) {
                    Text("28°")
                        .foregroundColor(.white)
                    Text("22°")
                        .foregroundColor(.white)
                }
                .padding(.trailing, 18)
            
            }
            .padding()
            .listRowInsets(EdgeInsets())
            .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .padding(0)
        }
        .listStyle(PlainListStyle())
        .background(Color(red: 0.01, green: 0.748, blue: 0.652))
        
    }
}

struct EightDaysView_Previews: PreviewProvider {
    static var previews: some View {
        EightDaysView()
    }
}

