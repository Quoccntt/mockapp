//
//  ContentView.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/08/2022.
//

import SwiftUI

struct ContentView: View {
    @State var timeRemaining = 3
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var isPush = false
    
    var body: some View {
        NavigationView {
            VStack(alignment: .center, spacing: 25.0) {
                Image("bg_undraw_weather")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding(.horizontal, 10.0)
                Text("Sử dụng ứng dụng MyWeather và nhận \n thông tin thời tiết hàng ngày, hàng giờ.")
                    .fontWeight(.semibold)
                    .foregroundColor(Color.black)
                NavigationLink("", isActive: $isPush, destination: {
                    MainHomeView()
                })
                Text("")
                    .onReceive(timer) { _ in
                        if timeRemaining > 0 {
                            timeRemaining -= 1
                        } else {
                           isPush = true
                        }
                    }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView()
    }
}
