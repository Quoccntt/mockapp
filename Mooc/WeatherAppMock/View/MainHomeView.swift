//
//  MainHomeView.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/08/2022.
//

import SwiftUI

struct MainHomeView: View {
    //    @State private var isShowMenu: Bool = tr
    @State private var selectedTab : Int = 0
    @StateObject var viewModel = WeatherViewModel()
    var body: some View {
        VStack {
            ZStack {
                VStack(alignment: .center) {
                    HStack(alignment: .center) {
                        Image("ic_menu")
                            .padding(.leading, 18)
                        Spacer()
                        
                        HStack(alignment: .center, spacing: 0) {
                            Spacer()
                            Image("ic_location")
                                .padding(.trailing, 10.0)
                            Text("Hà Nội")
                                .foregroundColor(.white)
                                .fontWeight(.heavy)
                            Spacer()
                        }
                        Spacer()
                        
                        Image("ic_search")
                            .padding(.trailing, 18)
                        
                    }.padding(.top, 44)
                        .padding(.bottom, 34)
                    
                    HStack(alignment: .center) {
                        TabBarView(selectedTab: $selectedTab, tabBarName: ["HÔM NAY", "NGÀY MAI", "8 NGÀY"])
                    }.padding(.top, 0)
                    
                    ZStack {
                        Rectangle().fill(Color.clear)
                        
                        switch selectedTab {
                        case 0:
                            if viewModel.weatherHourlyList.count ?? 0 > 0 {
                                TodayView(data: viewModel.weatherHourlyList.first!)
                                    .background(Color.white)
                                    .transition(.identity)
                            }
                            
                        case 1:
                            TomorowDay()
                                .background(Color.white)
                                .transition(.identity)
                        case 2:
                            EightDaysView()
                                .background(Color.white)
                                .transition(.identity)
                        default:
                            if viewModel.weatherHourlyList.count ?? 0 > 0 {
                                TodayView(data: viewModel.weatherHourlyList.first!)
                                    .background(Color.white)
                                    .transition(.identity)
                            }
                        }
                        
                        
                    }
                    
                    Spacer()
                }
                .navigationBarHidden(true)
                .frame(minWidth: 0, idealWidth: .greatestFiniteMagnitude, maxWidth: .infinity, minHeight: 0, idealHeight: .greatestFiniteMagnitude, maxHeight: .infinity, alignment: .center)
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
                .edgesIgnoringSafeArea(.all)
                
            }
        }
        .onAppear() {
            //call API
            viewModel.fetchWeatherByCoor(lat: "12", lon: "23")
        }
    }
    
}

struct MainHomeView_Previews: PreviewProvider {
    static var previews: some View {
        MainHomeView()
    }
}

