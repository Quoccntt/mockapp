//
//  AppService.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import Foundation

class AppService: NSObject {
    
    func getDataCities() -> [City] {
        let fileName: String = "data_cities"
        var result: [City] = [City]()
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let response = try? JSONDecoder().decode(CityResponse.self, from: data)
                result = response?.cities_list ?? [City]()
            } catch {
                
            }
        }
        return result
    }
}
