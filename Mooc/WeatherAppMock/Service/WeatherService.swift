//
//  WeatherService.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import Foundation
 
class WeatherService {
    func getWeatherByCoordinate(lat: String, lon: String, completion: @escaping (WeatherResponse?) -> ()) {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(StringConstant.API_KEY)&units=metric&lang=vi") else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let weatherResponse = try JSONDecoder().decode(WeatherResponse.self, from: data)
                completion(weatherResponse)
            } catch let error {
                print("----Occur error getWeatherByCoordinate : \(error.localizedDescription)")
                completion(nil)
            }
        }.resume()
    }
    
    func getWeatherHourlyByCoordinate(lat: String, lon: String, completion: @escaping (WeatherHourlyResponse?) -> ()) {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast/hourly?lat=\(lat)&lon=\(lon)&appid=\(StringConstant.API_KEY)&units=metric&lang=vi") else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let weatherResponse = try JSONDecoder().decode(WeatherHourlyResponse.self, from: data)
                completion(weatherResponse)
            } catch let error {
                print("----Occur error getWeatherHourlyByCoordinate : \(error.localizedDescription)")
                // dummy data
                let fileName: String = "hourly_weather"
                if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
                    do {
                        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                        let response = try? JSONDecoder().decode(WeatherHourlyResponse.self, from: data)
                        completion(response)
                    } catch {
                        print(error.localizedDescription)
                        completion(nil)
                    }
                }
            }
        }.resume()
    }
    
    func get8WeatherDayByCoordinate(lat: String, lon: String, completion: @escaping (EightDayWeatherResponse?) -> ()) {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast/daily?lat=\(lat)&lon=\(lon)&cnt=8&appid=\(StringConstant.API_KEY)&units=metric&lang=vi") else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let weatherResponse = try JSONDecoder().decode(EightDayWeatherResponse.self, from: data)
                completion(weatherResponse)
            } catch let error {
                print("----Occur error get8WeatherDayByCoordinate : \(error.localizedDescription)")
                // dummy data
                let fileName: String = "daily_weather"
                if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
                    do {
                        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                        let response = try? JSONDecoder().decode(EightDayWeatherResponse.self, from: data)
                        completion(response)
                    } catch {
                        print(error.localizedDescription)
                        completion(nil)
                    }
                }
            }
        }.resume()
    }
}
