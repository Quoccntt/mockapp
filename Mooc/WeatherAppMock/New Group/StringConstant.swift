
import Foundation

class StringConstant {
    static let API_KEY: String = "5daff8f1fa1502a8a79fa6d79f0f1139"
}

extension String {
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension Date {
    func dateAfterNumberOfdays(days: Int) -> Date {
        Calendar.current.date(byAdding: .day, value: days, to: Date()) ?? Date()
    }
}

extension NSNotification.Name {
    static let onCitySelected = Notification.Name("onCitySelected")
}

extension String {
    func getUrlStringIcon(iconName: String) -> String {
        return "http://openweathermap.org/img/wn/\(iconName)@2x.png"
    }
}

extension Double {
    func toCelsiusTemp(kelvinTemp: Double) -> String {
        let celsiusTemp = kelvinTemp - 273.15
        return String(format: "%.0f", celsiusTemp)
    }
}
