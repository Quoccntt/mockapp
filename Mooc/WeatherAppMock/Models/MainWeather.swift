//
//  MainWeather.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//


import Foundation

struct WeatherResponse: Decodable {
    let coord: Coord
    let weather: [Weather]
    let base: String?
    let main: MainWeather
    let visibility: Int?
    let wind: Wind
    let clouds: Clouds
    let dt: Int?
    let sys: SysWeather
    let timezone: Int?
    let id: Int?
    let name: String?
    let cod: Int?
}

struct MainWeather: Decodable {
    var temp: Double?
    var feels_like: Double?
    var temp_min: Double?
    var temp_max: Double?
    var pressure: Double?
    var humidity: Double?
    var sea_level: Double?
    var grnd_level: Double?
}

struct Weather: Decodable {
    var id: Int?
    var main, weatherDescription, icon: String?

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

struct Coord: Decodable {
    var lon: Double?
    var lat: Double?
}

struct Wind: Decodable {
    let speed: Double
    let deg: Int
    let gust: Double
}

struct Clouds: Decodable {
    var all: Double
}

struct SysWeather: Decodable {
    var type: Int?
    var id: Int?
    var country: String?
    var sunrise: Int?
    var sunset: Int?
}
// MARK: ======================== Hourly
struct WeatherHourlyResponse: Decodable {
    let cod: String
    let message, cnt: Int
    let city: CityWeatherHourly
    let list: [HourlyWeather]
}

struct HourlyWeather: Decodable {
    let dt: Int
    let main: MainWeatherHourly
    let weather: [Weather]
    let clouds: Clouds
    let wind: Wind
    let visibility: Int
    let pop: Double
    let rain: Rain?
    let sys: Sys
    let dtTxt: String

    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, visibility, pop, rain, sys
        case dtTxt = "dt_txt"
    }
    
    var hour: String {
        let date = dtTxt.toDate() ?? Date()
        let components = Calendar.current.dateComponents([.hour], from: date)
        let hour = components.hour ?? 0
        return String(hour)
    }
}

struct Rain: Codable {
    let the1H: Double

    enum CodingKeys: String, CodingKey {
        case the1H = "1h"
    }
}

struct Sys: Codable {
    let pod: String
}

struct MainWeatherHourly: Decodable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, seaLevel, grndLevel, humidity: Int
    let tempKf: Double

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
        case tempKf = "temp_kf"
    }
}

struct CityWeatherHourly: Decodable {
    let id: Int
    let name: String
    let coord: Coord
    let country: String
    let population, timezone, sunrise, sunset: Int
}

// MARK: ======================== Eight day
struct EightDayWeatherResponse: Codable {
    let city: EightDayCity
    let cod: String
    let message: Double
    let cnt: Int
    let list: [EightDayList]
}

// MARK: - City
struct EightDayCity: Codable {
    let id: Int
    let name: String
    let coord: EightDayCoord
    let country: String
    let population, timezone: Int
}

// MARK: - Coord
struct EightDayCoord: Codable {
    let lon, lat: Double
}

// MARK: - List
struct EightDayList: Codable {
    let dt, sunrise, sunset: Int
    let temp: Temp
    let feelsLike: FeelsLike
    let pressure, humidity: Int
    let weather: [WeatherEightDay]
    let speed: Double
    let deg: Int
    let gust: Double
    let clouds: Int
    let pop: Double
    let rain: Double?

    enum CodingKeys: String, CodingKey {
        case dt, sunrise, sunset, temp
        case feelsLike = "feels_like"
        case pressure, humidity, weather, speed, deg, gust, clouds, pop, rain
    }
}

// MARK: - FeelsLike
struct FeelsLike: Codable {
    let day, night, eve, morn: Double
}

// MARK: - Temp
struct Temp: Codable {
    let day, min, max, night: Double
    let eve, morn: Double
}

// MARK: - Weather
struct WeatherEightDay: Codable {
    let id: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

