//
//  City.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//


import Foundation

/*{
 "name": "Abidjan",
 "lon": "-4.00167",
 "lat": "5.35444",
 "country": "Ivory Coast"
}*/

struct CityResponse: Codable {
    var cities_list: [City]?
    
    enum CodingKeys: String, CodingKey {
        case cities_list = "cities_list"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        cities_list = try container.decodeArrayIfPresent(City.self, forKey: .cities_list)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeArrayIfPresent(cities_list, forKey: .cities_list)

    }
}

struct City: Codable, Identifiable, Hashable {
    var id: String{
        return name!
    }
    let name: String?
    let lon: String?
    let lat: String?
    let country: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case lon = "lon"
        case lat = "lat"
        case country = "country"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        lon = try container.decodeIfPresent(String.self, forKey: .lon)
        lat = try container.decodeIfPresent(String.self, forKey: .lat)
        country = try container.decodeIfPresent(String.self, forKey: .country)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(lon, forKey: .lon)
        try container.encodeIfPresent(lat, forKey: .lat)
        try container.encodeIfPresent(country, forKey: .country)
    }
}


extension KeyedEncodingContainerProtocol {

    public mutating func encodeArray<T>(_ values: [T], forKey key: Self.Key) throws where T : Encodable {
        var arrayContainer = nestedUnkeyedContainer(forKey: key)
        try arrayContainer.encode(contentsOf: values)
    }

    public mutating func encodeArrayIfPresent<T>(_ values: [T]?, forKey key: Self.Key) throws where T : Encodable {
        if let values = values {
            try encodeArray(values, forKey: key)
        }
    }

    public mutating func encodeMap<T>(_ pairs: [Self.Key: T]) throws where T : Encodable {
        for (key, value) in pairs {
            try encode(value, forKey: key)
        }
    }

    public mutating func encodeMapIfPresent<T>(_ pairs: [Self.Key: T]?) throws where T : Encodable {
        if let pairs = pairs {
            try encodeMap(pairs)
        }
    }

}

extension KeyedDecodingContainerProtocol {

    public func decodeArray<T>(_ type: T.Type, forKey key: Self.Key) throws -> [T] where T : Decodable {
        var tmpArray = [T]()
        var nestedContainer: Any?
        do {
            nestedContainer = try nestedUnkeyedContainer(forKey: key)
        }
        catch {
            return tmpArray
        }
        var nestedContainerUnkeyedDecoding = nestedContainer as! UnkeyedDecodingContainer
        while !nestedContainerUnkeyedDecoding.isAtEnd {
            let arrayValue = try nestedContainerUnkeyedDecoding.decode(T.self)
            tmpArray.append(arrayValue)
        }

        return tmpArray
    }

    public func decodeArrayIfPresent<T>(_ type: T.Type, forKey key: Self.Key) throws -> [T]? where T : Decodable {
        var tmpArray: [T]? = nil

        if contains(key) {
            tmpArray = try decodeArray(T.self, forKey: key)
        }

        return tmpArray
    }

    public func decodeMap<T>(_ type: T.Type, excludedKeys: Set<Self.Key>) throws -> [Self.Key: T] where T : Decodable {
        var map: [Self.Key : T] = [:]

        for key in allKeys {
            if !excludedKeys.contains(key) {
                let value = try decode(T.self, forKey: key)
                map[key] = value
            }
        }

        return map
    }

}
