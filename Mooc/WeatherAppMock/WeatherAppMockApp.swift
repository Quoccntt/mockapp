//
//  WeatherAppMockApp.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/08/2022.
//

import SwiftUI

@main
struct WeatherAppMockApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
